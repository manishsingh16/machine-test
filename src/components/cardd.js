import React, { useState } from "react";
import "./cardd.css";
import { Button } from "antd";
import { Card } from "antd";

const { Meta } = Card;

const Cardd = ({ data, onCompare }) => {
  return (
    <>
      <div className="card">
        <Card
          hoverable
          style={{ width: 240 }}
          cover={<img alt="example" src={data.url} />}
        >
          <Meta
            title={data.title}
            description={data.id}
            description={data.url}
          />
          <br />
          <Button type="primary" onClick={(e) => onCompare(e, data)}>
            Compare
          </Button>
        </Card>
      </div>
    </>
  );
};
export default Cardd;
