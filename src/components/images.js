import React from "react";
import Cardd from "./cardd";
import axios from "axios";
import "./api.css";
import DeleteOutlined from "@ant-design/icons";
const Images = () => {
  const [data, setData] = React.useState();
  const [tableData, setTableData] = React.useState([]);
  const [remove, setRemove] = React.useState([]);

 
  React.useEffect(() => {
  
      axios
        .get("https://jsonplaceholder.typicode.com/photos")
        .then((response) => {
          setData(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
  }, []);

  const compareFn = (images) => {
    const data = [...tableData, images];
    setTableData(data);
  };
  const deleteRow = (event, key) => {
    let data = [];
    let data_copy = [...tableData];
    data_copy.map((item) => {
      if (item.id !== key) {
        data = [...data, item];
      }
    });
    setTableData(data);
  };

  return (
    <>
      <div className="api">
        {/* <button onClick={() => showTableData()}>Manish</button> */}
        {data ? (
          data.map((hh, i) => {
            return (
              <Cardd
                key={i}
                data={hh}
                onCompare={(e, images) => compareFn(images)}
              />
            );
          })
        ) : (
          <h3>Loading...</h3>
        )}
      </div>
      <div id="table">
        <h2>Compare Table</h2>
        <table
          className="comparison-table"
          style={{
            width: "100%",
            border: "1px solid black",
            borderSpacing: "0px",
            padding: "10px",
            
          }}
        >
          <thead>
            <tr>
              <th>image</th>
              <th>id</th>
              <th>URL</th>
              <th>description</th>
              <th>remove</th>
            </tr>
          </thead>
          <tbody>
            {tableData.map((item, key) => {
              return (
                <tr>
                  <td>
                    <img src={item.url}></img>
                  </td>
                  <td>{item.id}</td>
                  <td>{item.url}</td>
                  <td>{item.title}</td>
                  <td>
                    <button onClick={(e) => deleteRow(e, item.id)}>
                      REMOVE
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Images;
