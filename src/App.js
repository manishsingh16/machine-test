import './App.css';
import Images  from './components/images';
import Table from './components/Table';

function App() {
  return (
    <div className="App">
      <Images />
    </div>
  );
}

export default App;
